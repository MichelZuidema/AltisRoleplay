/*

Easy Hostage script by dj otacon ver 3.0

Makes any unit in the game a hostage SP/MP

usage: _null=[this]execVM "scripts\hostage4.sqf"; // Change the path if you are using another script location
to the unit INIT

*/

// Set Variables
private ["_hostage","_callergroup"];

_hostage = _this select 0;
_callergroup = _this select 1; // Variable que almacena el grupo del llamador de la accion

// Actions
		// Making hostage captive
		_hostage setCaptive true;
		sleep 0.001;
		removeAllWeapons _hostage;
		removeAllAssignedItems _hostage;
		removeBackpack _hostage;
		//removeHeadgear _hostage;
		removeVest _hostage;
		//_hostage setUnitPos "middle";  // falla en la ver 1.68
		_hostage disableAI "autoTarget";
		_hostage setBehaviour "Careless";
		_hostage allowFleeing 0;
		_hostage disableAI "Move";
		_hostage  = _hostage;
if (isdedicated) then {
		[_hostage,["<t color=""#F5AE16"">" + ("Secure prisioner") + "</t>",{
			//private ["_hostage","_callergroup"];
			//_hostage = _this select 3 select 0; // Hay que redefinir la variable
			//_hostage stop true;
			//_hostage setUnitPos "down";
			/*
			(_this select 0) stop true;
			//(_this select 0) setUnitPos "down";

			(_this select 0) enableAI "MOVE";
			(_this select 0) enableAI "AUTOTARGET";
			(_this select 0) enableAI "ANIM";
			(_this select 0) setBehaviour "Careless";
			(_this select 0) setCaptive false;
			//(_this select 0) setUnitPos "UP";
			(_this select 0) setskill 1;
			[(_this select 0)] joinSilent (group player);
			*/
			 "Securing hostage..." remoteExec ["hintsilent" ,0];
			[(_this select 0),(_this select 2)] remoteExec ["removeAction" ,[0,-2] select isDedicated,true];
			//removeAllActions _hostage;
			sleep 3;
			(_this select 0) setBehaviour "CARELESS";
			(_this select 0) enableAI "Move";
			(_this select 0) setskill 1;
			[(_this select 0)] joinsilent (group player);
			"Hostage secured!" remoteExec ["hintsilent" ,0];
			//hint "Hostage secured!";
			[(_this select 0),["<t color=""#F5AE16"">" + ("Free hostage") + "</t>",{
				//(_this select 0) = _this select 0; // Hay que redefinir la variable
				//(_this select 0) stop false;
				//(_this select 0) setUnitPos "UP";
				//(_this select 0) setBehaviour "CARELESS";
				(_this select 0) enableAI "Move";
				(_this select 0) allowFleeing 1;
				//removeallactions _hostage;
				[(_this select 0)] join grpNull;
				//hint "Hostage released...";
				"Hostage released..." remoteExec ["hintsilent",0];
				[(_this select 0),(_this select 2)] remoteExec ["removeAction" ,[0,-2] select isDedicated,true];
				},nil,6,true,true,"","_this distance _target < 3"]] remoteExec ["addaction",[0,-2] select isDedicated,true];
			},nil,6,true,true,"","_this distance _target < 3"]] remoteExec ["addaction",[0,-2] select isDedicated,true];
		} else {
			if  (isServer) then {

				_hostage addaction ["<t color=""#F5AE16"">" + ("Secure prisioner") + "</t>",{
				_hostage = _this select 0; // Hay que redefinir la variable
				_hostage stop true;
				_hostage setUnitPos "down";
				hintsilent "Securing hostage...";
				removeAllActions _hostage;
				sleep 3;
				hint "Hostage secured!";
				_hostage addaction ["<t color=""#F5AE16"">" + ("Follow squad") + "</t>",{
						_hostage = _this select 0; // Hay que redefinir la variable
						_callergroup = _this select 1;
						_hostage stop false;
						_hostage setUnitPos "UP";
						//_hostage switchmove "";
						_hostage setCaptive false;
						_hostage setBehaviour "CARELESS";
						_hostage enableAI "Move";
						_hostage setskill 1; // añadido para mejorar el comportamiento del prisionero
						[_hostage] joinsilent _callergroup;
						hintsilent "Following the squad..";
						sleep 1;
						removeAllActions _hostage;
						_hostage addaction ["<t color=""#F5AE16"">" + ("Free hostage") + "</t>",{
								_hostage = _this select 0; // Hay que redefinir la variable
								_hostage stop false;
								_hostage setUnitPos "UP";
								_hostage setBehaviour "CARELESS";
								_hostage enableAI "Move";
								_hostage allowFleeing 1;
								removeallactions _hostage;
								[_hostage] join grpNull;
								hint "Hostage released...";
								},nil,6,true,true,"","_this distance _target < 3"];
						},nil,6,true,true,"","_this distance _target < 3"];
				},nil,6,true,true,"","_this distance _target < 3"];
			};
		};
