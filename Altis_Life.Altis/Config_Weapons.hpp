/*
*    Format:
*        level: ARRAY (This is for limiting items to certain things)
*            0: Variable to read from
*            1: Variable Value Type (SCALAR / BOOL / EQUAL)
*            2: What to compare to (-1 = Check Disabled)
*            3: Custom exit message (Optional)
*
*    items: { Classname, Itemname, BuyPrice, SellPrice }
*
*    Itemname only needs to be filled if you want to rename the original object name.
*
*    Weapon classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Weapons
*    Item classnames can be found here: https://community.bistudio.com/wiki/Arma_3_CfgWeapons_Items
*
*/
class WeaponShops {
    //Armory Shops
    class gun {
        name = "Fire Fuck Firearms";
        side = "civ";
        license = "gun";
        level[] = { "", "", -1, "" };
        items[] = {
            { "hgun_Rook40_F", "", 1750, 500 },
            { "hgun_Pistol_heavy_02_F", "", 2100, -1 },
            { "hgun_ACPC2_F", "", 2500, -1 }
        };
        mags[] = {
            { "16Rnd_9x21_Mag", "", 150 },
            { "6Rnd_45ACP_Cylinder", "", 200 },
            { "9Rnd_45ACP_Mag", "", 185 }
        };
    };

    class rebel {
        name = "Mohammed's Jihadi Shop";
        side = "civ";
        license = "rebel";
        level[] = { "", "", -1, "" };
        items[] = {
            { "arifle_TRG20_F", "", 40000, 4000 },
            { "arifle_Katiba_F", "", 80000, 8000 },
            { "arifle_MX_F", "", 120000, 12000},
            { "arifle_MXC_F", "", 110000, 11000},
            { "arifle_MXM_F", "", 150000, 15000},
            { "arifle_ARX_blk_F", "", 250000, 25000},
            { "optic_ACO_grn", "", 5000, 500 },
            { "optic_Holosight", "", 6500, 650 },
            { "acc_flashlight", "", 12000, 1200 },
            { "muzzle_snds_H", "", 15000, 1500},
            { "acc_pointer_IR", "", 15000, 1500},
            { "optic_Hamr", "", 25000, 2500 },
            { "optic_Arco", "", 30000, 3000},
            { "optic_ERCO_blk_F", "", 30000, 3000}
        };
        mags[] = {
            { "30Rnd_556x45_Stanag", "", 300 },
            { "30Rnd_65x39_caseless_green", "", 350 },
            { "30Rnd_65x39_caseless_mag_Tracer", "", 450 }
        };
    };

    class gang {
        name = "Hideout Armament";
        side = "civ";
        license = "";
        level[] = { "", "", -1, "" };
        items[] = {
            { "hgun_Rook40_F", "", 1750, 500 },
            { "hgun_Pistol_heavy_02_F", "", 2100, -1 },
            { "hgun_ACPC2_F", "", 2500, -1 }
        };
        mags[] = {
            { "16Rnd_9x21_Mag", "", 150 },
            { "6Rnd_45ACP_Cylinder", "", 200 },
            { "9Rnd_45ACP_Mag", "", 185 }
        };
    };

    //Basic Shops
    class genstore {
        name = "Altis General Store";
        side = "civ";
        license = "";
        level[] = { "", "", -1, "" };
        items[] = {
            { "Binocular", "", 150, -1 },
            { "ItemGPS", "", 100, 45 },
            { "ItemMap", "", 50, 35 },
            { "ItemCompass", "", 50, 25 },
            { "ItemWatch", "", 50, -1 },
            { "ToolKit", "", 250, 75 },
            { "FirstAidKit", "", 150, 65 },
            { "NVGoggles", "", 2000, 980 },
            { "Chemlight_red", "", 300, -1 },
            { "Chemlight_yellow", "", 300, 50 },
            { "Chemlight_green", "", 300, 50 },
            { "Chemlight_blue", "", 300, 50 }
        };
        mags[] = {};
    };

    class f_station_store {
        name = "Altis Fuel Station Store";
        side = "";
        license = "";
        level[] = { "", "", -1, "" };
        items[] = {
            { "Binocular", "", 750, -1 },
            { "ItemGPS", "", 500, 45 },
            { "ItemMap", "", 250, 35 },
            { "ItemCompass", "", 250, 25 },
            { "ItemWatch", "", 250, -1 },
            { "ToolKit", "", 1250, 75 },
            { "FirstAidKit", "", 750, 65 },
            { "NVGoggles", "", 10000, 980 },
            { "Chemlight_red", "", 1500, -1 },
            { "Chemlight_yellow", "", 1500, 50 },
            { "Chemlight_green", "", 1500, 50 },
            { "Chemlight_blue", "", 1500, 50 }
        };
        mags[] = {};
    };

    //Cop Shops
    class cop_basic {
        name = "Altis Cop Shop";
        side = "cop";
        license = "";
        level[] = { "", "", -1, "" };
        items[] = {
            { "arifle_Mk20_F", "", 0, 0},
            { "hgun_P07_snds_F", "Stun Pistol", 0, 0 },
            { "Binocular", "", 0, -1 },
            { "ItemGPS", "", 0, 0 },
            { "ToolKit", "", 0, 0 },
            { "muzzle_snds_L", "", 0, -1 },
            { "FirstAidKit", "", 0, 0 },
            { "Medikit", "", 0, 0 },
            { "NVGoggles", "", 0, 0 },
            { "acc_flashlight", "", 0, 0 },
            { "optic_Holosight", "", 0, 0 },
            { "optic_Arco", "", 0, -1 },
            { "optic_Hamr", "", 0, -1 },
            { "muzzle_snds_H", "", 0, -1 }
        };
        mags[] = {
            { "30Rnd_556x45_Stanag", "", 0},
            { "16Rnd_9x21_Mag", "", 0 },
            { "20Rnd_556x45_UW_mag", "Taser Rifle Magazine", 0 }
        };
    };

    class cop_patrol {
        name = "Altis Patrol Officer Shop";
        side = "cop";
        license = "";
        level[] = { "life_coplevel", "SCALAR", 2, "You must be a Patrol Officer Rank!" };
        items[] = {
            { "arifle_SPAR_01_blk_F", "", 0, 0},
            { "SMG_02_ACO_F", "", 0, -1 },
            { "acc_flashlight", "", 0, 0 },
            { "optic_Holosight", "", 0, 0 },
            { "optic_Arco", "", 0, -1 },
            { "optic_Hamr", "", 0, -1 },
            { "muzzle_snds_H", "", 0, -1 }
        };
        mags[] = {
            { "30Rnd_556x45_Stanag", "", 0},
            { "30Rnd_65x39_caseless_mag", "", 0 },
            { "30Rnd_9x21_Mag", "", 0 }
        };
    };

    class cop_sergeant {
        name = "Altis Sergeant Officer Shop";
        side = "cop";
        license = "";
        level[] = { "life_coplevel", "SCALAR", 5, "You must be a Sergeant Rank!" };
        items[] = {
            { "SMG_02_ACO_F", "", 0, -1 },
            { "arifle_MX_Black_F", "", 0, 0 },
            { "arifle_MXC_Black_F", "", 0, 0 },
            { "arifle_ARX_blk_F", "", 0, 0 },
			{ "srifle_DMR_03_MRCO_F", "", 0, 0 },	
            { "acc_flashlight", "", 0, 0 },
            { "optic_Holosight", "", 0, 0 },
            { "optic_Hamr", "", 0, 0 },
            { "optic_Arco", "", 0, -1 },
			{ "optic_ERCO_blk_F", "", 0, -1 },
            { "muzzle_snds_H", "", 0, -1 },
            { "muzzle_snds_65_TI_blk_F", "", 0, -1 }
			{ "muzzle_snds_B", "", 0, -1 }
			{ "bipod_01_F_blk", "", 0, -1 }
			

        };
        mags[] = {
            { "9Rnd_45ACP_Mag", "", 0 },
            { "30Rnd_9x21_Mag", "", 0 },
            { "30Rnd_65x39_caseless_mag", "", 0, -1 },
            { "30Rnd_65x39_caseless_green", "", 0, -1 },
			{ "20Rnd_762x51_Mag", "", 0, -1 }
			
        };
    };

    //Medic Shops
    class med_basic {
        name = "store";
        side = "med";
        license = "";
        level[] = { "", "", -1, "" };
        items[] = {
            { "ItemGPS", "", 0, 0 },
            { "Binocular", "", 0, -1 },
            { "ToolKit", "", 0, 0 },
            { "FirstAidKit", "", 0, 0 },
            { "Medikit", "", 0, 0 },
            { "NVGoggles", "", 0, 0 }
        };
        mags[] = {};
    };
};